## The Fire Wires @ Gears
### FTC Team 12014   

This is the competition code for Rover Ruckus the space themed 2018/2019 FTC Game.

Changelog:

**09/27/2018**
 * Initial Import from SDK 4.0
 * Include OpenCVLibrary3 Module
 * Include DogeCV
 * Include Joystick Conditioning into newly created FireBot Library