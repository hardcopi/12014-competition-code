/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import org.firstinspires.ftc.teamcode.Libraries.Hardware;
import org.firstinspires.ftc.teamcode.Libraries.FireBot;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

@TeleOp(name = "Teleop Mode", group = "FireBot")
public class Teleop extends OpMode {

    /* Declare OpMode members. */
    FireBot firebot = new FireBot();
    Hardware robot = new Hardware();

    @Override
    public void init(){

        robot.init(hardwareMap);
    }

    @Override
    public void start(){

    }

    @Override
    public void loop() {


        telemetry.addData("Status", "Initialized");
        telemetry.update();




            final double LIMIT_STOP = 0.0;
            double leftPower;
            double rightPower;
            double hangElevatorPower;

            leftPower = -gamepad1.right_stick_y;
            rightPower = gamepad1.left_stick_y;
            hangElevatorPower = gamepad2.right_stick_y;


            robot.leftFront.setPower(leftPower);
            robot.leftBack.setPower(leftPower);
            robot.rightFront.setPower(rightPower);
            robot.rightBack.setPower(rightPower);

            if (robot.hangLimitDetectorTop.getState()) {
                telemetry.addData("Top Hang Limit Detector", "Is not Pressed");
                robot.hangElevator.setPower(hangElevatorPower);

            } else  {
                telemetry.addData("Top Hang Limit Detector", "Is Pressed");
                robot.hangElevator.setPower(LIMIT_STOP);
            }
        telemetry.addData("Top Hang Limit Detector", robot.hangLimitDetectorTop.getState());

        robot.hangElevator.setPower(hangElevatorPower);


        telemetry.update();


    }

    @Override
    public void stop(){

    }
}